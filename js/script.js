$(function(){
    $('.complex__result_tab li:first').addClass('select');                
    $('.complex__result_panels>div').hide().filter(':first').show();     
    $('.complex__result_tab a').click(function(){                         
      $('.complex__result_panels>div').hide().filter(this.hash).show();   
      $('.complex__result_tab li').removeClass('select');                 
      $(this).parent().addClass('select');                    
      return (false);                                        
    })
  })